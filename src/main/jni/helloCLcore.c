/*Copyright 2013 Rahul Garg

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.*/

#include <CL/cl.h>
#include <dlfcn.h>
#include <stdio.h>
#include <string.h>
#include <android/log.h>

static int loadedCL;

void *getCLHandle(){
	void *res = NULL;
    if( sizeof( void* ) == 8 ) {
        res = dlopen( "/system/lib64/libOpenCL.so", RTLD_LAZY );
        if ( res == NULL) {
            res = dlopen( "/system/vendor/lib64/egl/libGLES_mali.so", RTLD_LAZY );
        }
        if ( res == NULL ) {
            res = dlopen( "/system/vendor/lib64/libOpenCL.so", RTLD_LAZY );
        }
        if ( res == NULL ) {
            res = dlopen( "/system/vendor/lib64/libPVROCL.so", RTLD_LAZY );
        }
        if ( res == NULL) {
            res = dlopen( "/system/lib64/libllvm-a3xx.so", RTLD_LAZY );
        }
    } else {
        res = dlopen( "/system/lib/libOpenCL.so", RTLD_LAZY );
        if ( res == NULL) {
            res = dlopen( "/system/vendor/lib/egl/libGLES_mali.so", RTLD_LAZY );
        }
        if ( res == NULL ) {
            res = dlopen( "/system/vendor/lib/libOpenCL.so", RTLD_LAZY );
        }
        if ( res == NULL ) {
            res = dlopen( "/system/vendor/lib/libPVROCL.so", RTLD_LAZY );
        }
        if ( res == NULL) {
            res = dlopen( "/system/lib/libllvm-a3xx.so", RTLD_LAZY );
        }
    }
	if(res==NULL) __android_log_print( ANDROID_LOG_ERROR, "JNI", "Could not open library :(");
	else __android_log_print( ANDROID_LOG_INFO, "JNI", "loaded some library" );
	return res;
}

cl_int (*rclGetPlatformIDs)(cl_uint          /* num_entries */,
                 cl_platform_id * /* platforms */,
                 cl_uint *        /* num_platforms */);


cl_int (*rclGetPlatformInfo)(cl_platform_id   /* platform */, 
                  cl_platform_info /* param_name */,
                  size_t           /* param_value_size */, 
                  void *           /* param_value */,
                  size_t *         /* param_value_size_ret */);

cl_int (*rclGetDeviceIDs)(cl_platform_id   /* platform */,
               cl_device_type   /* device_type */, 
               cl_uint          /* num_entries */, 
               cl_device_id *   /* devices */, 
               cl_uint *        /* num_devices */);


cl_int (*rclGetDeviceInfo)(cl_device_id    /* device */,
                cl_device_info  /* param_name */, 
                size_t          /* param_value_size */, 
                void *          /* param_value */,
                size_t *        /* param_value_size_ret */);


void initFns(){
	loadedCL = 0;
	void *handle = getCLHandle();
	if(handle==NULL) return;
	rclGetPlatformIDs = (cl_int (*)(cl_uint,cl_platform_id *,cl_uint*))dlsym(handle,"clGetPlatformIDs");
	rclGetPlatformInfo = (cl_int (*)(cl_platform_id, cl_platform_info, size_t, void *, size_t*))dlsym(handle,"clGetPlatformInfo");
	rclGetDeviceIDs = (cl_int (*)(cl_platform_id, cl_device_type, cl_uint, cl_device_id *, cl_uint*))dlsym(handle,"clGetDeviceIDs");
	rclGetDeviceInfo = (cl_int (*)(cl_device_id, cl_device_info, size_t, void *, size_t*))dlsym(handle,"clGetDeviceInfo");
    loadedCL = 1;
    if ( rclGetDeviceIDs == NULL || rclGetDeviceInfo == NULL || rclGetPlatformIDs == NULL || rclGetPlatformInfo == NULL ) {
        __android_log_print( ANDROID_LOG_ERROR, "JNI", "Library does not have OpenCL functions :-(");
        loadedCL = 0;
    }
}


const char *getResultString(){
	initFns();
	if(loadedCL==0){
		const char *ptr = "Did not find OpenCL\n";
		return ptr;
	}
	cl_platform_id platforms[10];
	cl_uint numPlats;
	rclGetPlatformIDs(10,platforms,&numPlats);
	int i;
	char *result = (char*)malloc(10000);
	result[0] = '\0';
	int index = 0;

    int cnt = sprintf( &result[ index ], "OpenCL found!\n\n" );
    index += cnt;

	for(i=0;i<numPlats;i++){	
			char platname[100] = {0};
			rclGetPlatformInfo(platforms[i],CL_PLATFORM_NAME,sizeof(platname),platname,NULL);
			int count = sprintf(&result[index],"Platform name: %s\n",platname);
			index += count;

			char platversion[100] = {0};
			rclGetPlatformInfo(platforms[i],CL_PLATFORM_VERSION,sizeof(platversion),platversion,NULL);
			count = sprintf(&result[index],"Platform version: %s\n",platversion);
			index += count;

			cl_device_id devices[10];
			cl_uint ndevices;
			rclGetDeviceIDs(platforms[i],CL_DEVICE_TYPE_GPU,10,devices,&ndevices);
			int j;

            count = sprintf( &result[index], "\nGPU devices: \n" );
            index += count;

			for(j=0;j<ndevices;j++){
					char devname[100] = {0};
					rclGetDeviceInfo(devices[j],CL_DEVICE_NAME,sizeof(devname),devname,NULL);
					count = sprintf(&result[index],"Device %d name: %s\n", j ,devname);
					index += count;
			}
	}
	return result;
}

