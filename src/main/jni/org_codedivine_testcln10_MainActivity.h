/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class org_codedivine_testcln10_MainActivity */

#ifndef _Included_org_codedivine_testcln10_MainActivity
#define _Included_org_codedivine_testcln10_MainActivity
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     org_codedivine_testcln10_MainActivity
 * Method:    testCL
 * Signature: ()Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_org_codedivine_testcln10_MainActivity_testCL
  (JNIEnv *, jobject);

#ifdef __cplusplus
}
#endif
#endif
